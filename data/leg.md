| Category | Icon |
|----|----|
| Spawn point | ![Spawn point](img/ico/todo.webp) |
| Control Point | ![Control Point](img/ico/todo.webp) |
| FIXME UNASSIGNED | ![FIXME UNASSIGNED](img/ico/todo.webp) |
| Static Ammo Crate | ![Static Ammo Crate](img/ico/ammo.webp) |
| Ammo Kit | ![Ammo Kit](img/ico/ammo.webp) |
| Tankhunter Kit | ![Tankhunter Kit](img/ico/boom.webp) |
| Deployable Arty | ![Deployable Arty](img/ico/arty_dep.webp) |
| Assault Kit | ![Assault Kit](img/ico/assault.webp) |
| Commando Kit | ![Commando Kit](img/ico/sword.webp) |
| Easteregg | ![Easteregg](img/ico/easteregg.webp) |
| Engineer Kit | ![Engineer Kit](img/ico/engineer.webp) |
| MG Kit | ![MG Kit](img/ico/todo.webp) |
| Deployable MG | ![Deployable MG](img/ico/todo.webp) |
| Sniper Kit | ![Sniper Kit](img/ico/sniper.webp) |
| Medic Kit | ![Medic Kit](img/ico/medic.webp) |
| Parachute Kit | ![Parachute Kit](img/ico/para.webp) |
| Flamethrower Kit | ![Flamethrower Kit](img/ico/flame.webp) |
| HEAT Thrower | ![HEAT Thrower](img/ico/rocket.webp) |
| AT Rifle | ![AT Rifle](img/ico/todo.webp) |
| Artillery | ![Artillery](img/ico/arty.webp) |
| Anti-tank Gun | ![Anti-tank Gun](img/ico/pak.webp) |
| Static MG | ![Static MG](img/ico/todo.webp) |
| Anti-aircraft Gun | ![Anti-aircraft Gun](img/ico/flak.webp) |
| Radio | ![Radio](img/ico/todo.webp) |
| Car | ![Car](img/ico/car.webp) |
| APC | ![APC](img/ico/apc.webp) |
| Tank | ![Tank](img/ico/tank.webp) |
| Airplane | ![Airplane](img/ico/plane.webp) |
| Ship | ![Ship](img/ico/boat.webp) |
| Mobile Arty | ![Mobile Arty](img/ico/arty.webp) |
| Mobile PaK | ![Mobile PaK](img/ico/pak.webp) |
| Mobile FlaK | ![Mobile FlaK](img/ico/flak.webp) |
| Supply Vehicle | ![Supply Vehicle](img/ico/engineer.webp) |
| Civilian Vehicle | ![Civilian Vehicle](img/ico/car.webp) |
| Scout Vehicle | ![Scout Vehicle](img/ico/binoc.webp) |
