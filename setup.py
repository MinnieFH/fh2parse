#!/usr/bin/env python
# vim:set ts=4 sts=4 sw=4 et:
from setuptools import setup

with open('requirements.txt') as fobj:
    requirements = [l.strip() for l in fobj.readlines()]

setup(
    name='fh2parse',
    description='FH2 .con file parsing',
    author='Pozzo',
    license='BSD-3',
    packages=[
        'fh2parse',
    ],
    python_requires='>=3.6',
    install_requires=requirements,
)
