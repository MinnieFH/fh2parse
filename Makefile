PKGNAME=fh2parse

.PHONY: default
default: build

.PHONY: default
all: install

.PHONY: build
build: 
	@echo "No need to build anymore :)"

.PHONY: install
install: dependencies
	pip install .

.PHONY: install-dev
install-dev: dependencies
	pip install -e .

.PHONY: clean
clean:
	python setup.py clean --all

.PHONY: test
test: 
	py.test  $(PKGNAME)

.PHONY: flake8
flake8: 
	py.test --flake8

.PHONY: pep8
pep8: flake8

.PHONY: docstyle
docstyle: 
	py.test --docstyle

.PHONY: lint
lint: 
	py.test --pylint

.PHONY: dependencies
dependencies:
	pip install -Ur requirements.txt

