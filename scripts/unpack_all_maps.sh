#!/usr/bin/env bash

unpack_all_maps() {
  while read l; 
  do 
    ./unpack_map.sh "$l";  
  done < ../data/mapnames.txt
}

unpack_all_maps
