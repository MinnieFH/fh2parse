#!/usr/bin/env bash

help="
Usage:
  list_all_gpo.sh [options] 

Options:
  -h, --help    Show help
  --verbose     Show more messages
  -g <layer>    Map layer [Default: 64]
"
eval "$(docopts -A args -h "$help" : "$@")"

while read l; 
do 
  echo "Reading GPO from" "$l"
  if [ "${args['--verbose']}" == true ] ; then
    ./list_gpo.py --verbose -g "${args['-g']}" "$l";  
  else
    ./list_gpo.py -g "${args['-g']}" "$l";  
  fi
done < ../data/mapnames.txt
