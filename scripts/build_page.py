#!/usr/bin/env python
# pylint: disable=W0232,C0103,C0111,F0401
# vim:set ts=4 sts=4 sw=4 et syntax=python:
"""
Build a page

Usage:
    build_page.py [options] MAPNAME

Options:
    -v --verbose                Print more output
    -l <ly>, --layer <ly>       Layer [Default: 64]
    -o <dir>, --outdir <dir>    Output directory. Defaults to the `page`
                                directory above
"""

ALL_COLS = [
    'gpo_subcat',
    'gpo_cat',
    'gpo_name',
    'pos_x',
    'pos_y',
    'pos_z',
    'flag',
    'is_locked',
    'team',
    'instance',
    'gpo_cat_disp',
    'gpo_subcat_disp',
]
MKD_COLS = [
    'gpo_subcat_disp',
    'gpo_name',
    'instance',
]


def build_page(mapname, layer='64', outdir=None, verbose=False):
    from os.path import join, exists
    from os import sep

    import numpy as np
    import pandas as pd

    from fh2parse.wiki import html_gallery
    from fh2parse import LEVEL_DIR, PAGE_DIR, GPO_CSV, IMG_DIR
    from fh2parse.gpo import GPOTable, SUBCAT_ICONS, censor_df
    from fh2parse.map import MiniMap, bounding_rectangle
    from fh2parse.parse import (
        ammo_as_gpo, parse_ammo, parse_object_spawns, read_confile,
        read_description, parse_heightmap_size, parse_combatareas
    )

    if not outdir:
        if verbose:
            print('Setting outdir to ', PAGE_DIR)
        outdir = PAGE_DIR
    outfile = join(outdir, '{}_{}.md'.format(mapname, layer))

    mapdir = join(LEVEL_DIR, mapname)
    if not exists(mapdir):
        raise FileNotFoundError(
            "{} does not exist, aborting...".format(mapdir)
        )

    gpo_con = join(mapdir, 'gamemodes', 'gpm_cq', layer, 'gameplayobjects.con')
    if not exists(gpo_con):
        raise FileNotFoundError(
            "{} does not exist, aborting...".format(gpo_con)
        )

    static_con = join(mapdir, 'staticobjects.con')
    if not exists(static_con):
        raise FileNotFoundError(
            "{} does not exist, aborting...".format(static_con)
        )

    heightdata_con = join(mapdir, 'heightdata.con')
    if not exists(heightdata_con):
        raise FileNotFoundError(
            "{} does not exist, aborting...".format(heightdata_con)
        )

    ddsname = join(mapdir, 'hud', 'minimap', 'ingamemap.dds')
    if not exists(ddsname):
        raise FileNotFoundError(
            "{} does not exist, aborting...".format(ddsname)
        )

    map_size = parse_heightmap_size(read_confile(heightdata_con))
    gpo_lines = read_confile(gpo_con, strip_empty_lines=False)
    spawned_objs = parse_object_spawns(gpo_lines)
    ammo_crates = parse_ammo(read_confile(static_con))
    spawned_objs.extend(ammo_as_gpo(ammo_crates))

    gpo_tab = GPOTable(GPO_CSV)
    objs = [gpo_tab.amend(ob) for ob in spawned_objs]

    df = pd.DataFrame.from_dict(objs)
    # use experimental string dtype instead of silly object dtype
    df = df.convert_dtypes()
    df = df[ALL_COLS]
    df = df.sort_values(by=['gpo_cat', 'gpo_subcat'])
    df = censor_df(df)
    try:
        disp_name, map_desc = read_description(mapname)
    except ValueError:
        disp_name = '<Unnamed Map>'
        map_desc = '...'
    disp_name += ' {}'.format(layer)
    out_md = "---\ntitle: {}\n---\n\n".format(disp_name)
    out_md += "{descript}\n\n".format(descript=map_desc, )

    areas = parse_combatareas(gpo_lines)
    del areas['time_to_pain']
    if 'damage' in areas:
        del areas['damage']
    all_points = []
    for areaname, vals in areas.items():
        # print(areaname)
        # print(vals)
        points = vals['points']
        all_points.extend(points)
    do_crop = True
    if not all_points:
        do_crop = False
    if do_crop:
        all_points = np.array(all_points)
        xmin, ymin, xmax, ymax = bounding_rectangle(all_points)

    cats = df['gpo_cat'].unique()
    image_files = {}
    for cat in cats:
        if cat == 'noidea':
            continue
        mini = MiniMap(ddsname, size=map_size)
        cat_df = df[df['gpo_cat'] == cat]
        subcats = cat_df['gpo_subcat'].unique()
        for sc in subcats:
            subcat_df = cat_df[cat_df['gpo_subcat'] == sc]
            for _, row in subcat_df.iterrows():
                pos = row[['pos_x', 'pos_y', 'pos_z']]
                # mini.add_marker(pos)
                ico_name = SUBCAT_ICONS[sc]
                try:
                    mini.add_icon(pos, ico_name)
                except ValueError:
                    # Image.alpha_composite raises when pos outsite bounds
                    continue
        imgname = join(
            outdir, 'img', '{}_{}_{}.webp'.format(mapname, layer, cat)
        )
        if do_crop:
            mini.crop(xmin, xmax, ymin, ymax)
        mini.draw_map(imgname)
        img_fname_html = join(
            'img', '{}_{}_{}.webp'.format(mapname, layer, cat)
        )
        descr = "{}".format(gpo_tab.get_display_name(cat))
        image_files[img_fname_html] = descr

    # mkd_table = df[MKD_COLS].to_markdown(showindex=False)
    mkd_table = df.to_markdown(showindex=False)
    out_md += html_gallery(image_files)
    out_md += "\n\n"
    out_md += mkd_table
    out_md += "\n\n"

    with open(outfile, 'w') as f:
        f.write(out_md)


if __name__ == '__main__':
    from docopt import docopt
    args = docopt(__doc__)
    mapname_ = args['MAPNAME']
    layer_ = args['--layer']
    verbose_ = args['--verbose']
    print("Building {} {}...".format(mapname_, layer_), end=' ')
    try:
        build_page(mapname_, layer=layer_, verbose=verbose_)
    except FileNotFoundError:
        print("ERROR: Missing files for {} {}".format(mapname_, layer_))
    else:
        print("OK!")
