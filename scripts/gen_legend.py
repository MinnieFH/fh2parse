#!/usr/bin/env python
# pylint: disable=W0232,C0103,C0111
# vim:set ts=4 sts=4 sw=4 et syntax=python:
"""
Build a legend for the main page.

Usage:
    gen_legend.py [options] OUTFILE

Options:
    -v, --verbose               Print more output
    -h, --help                  Show this help
    """


def gen_legend(outfile):
    from os.path import join

    from fh2parse.gpo import SUBCAT_ICONS, DISPLAY_NAMES

    out_md = '| Category | Icon |\n'
    out_md += '|----|----|\n'
    for gpo_name, img_name in SUBCAT_ICONS.items():
        disp_name = DISPLAY_NAMES[gpo_name]
        img_fullpath = join('img', 'ico', img_name)
        out_md += '| {dn} | ![{dn}]({im}) |\n'.format(
            dn=disp_name, im=img_fullpath
        )

    with open(outfile, 'w') as f:
        f.write(out_md)


if __name__ == '__main__':
    from docopt import docopt
    args = docopt(__doc__)
    verbose_ = args['--verbose']
    outfile_ = args['OUTFILE']
    print("Generating Legend...", end=' ')
    gen_legend(outfile_)
    print("OK!")
