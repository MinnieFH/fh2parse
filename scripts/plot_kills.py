#!/usr/bin/env python
# pylint: disable=W0232,C0103,C0111,F0401
# vim:set ts=4 sts=4 sw=4 et syntax=python:
"""
Plot all the kills

Usage:
    plot_kills.py [options] LOGFILE MAPNAME LAYER

Options:
    -v --verbose                Print more output
"""


def parse_evt(evt):
    team_att = int(evt['attacker']['playerteam'])
    team_vic = int(evt['defender']['playerteam'])
    pos_att = [float(v) for v in evt['attacker']['position'].split(',')]
    pos_vic = [float(v) for v in evt['defender']['position'].split(',')]
    veh_att = evt['attacker']['vehicle']
    weap = evt['attacker']['weapon']
    name_att = evt['attacker']['playername']
    name_vic = evt['defender']['playername']
    return {
        'team_att': team_att,
        'team_vic': team_vic,
        'pos_att_x': pos_att[0],
        'pos_att_y': pos_att[1],
        'pos_att_z': pos_att[2],
        'pos_vic_x': pos_vic[0],
        'pos_vic_y': pos_vic[1],
        'pos_vic_z': pos_vic[2],
        'name_att': name_att,
        'name_vic': name_vic,
        'veh_att': veh_att,
        'weap': weap,
    }


def read_json(fname, layer=64):
    import pandas as pd
    import json
    lines = []
    in_layer = False
    with open(fname) as infile:
        for line in infile.readlines():
            if '"INIT"' in line:
                if '"{}"'.format(layer) in line:
                    in_layer = True
                    continue
                else:
                    in_layer = False
                    continue
            if not in_layer:
                continue
            lines.append(line)

    lines = [line for line in lines if 'KILL' in line]
    evts = [json.loads(line) for line in lines]

    kills = [parse_evt(evt) for evt in evts]
    df = pd.DataFrame(kills)
    # use experimental strings
    df = df.convert_dtypes()
    return df


def filter_df(df):
    # no suicides
    df = df[df['name_att'] != df['name_vic']]
    # no teamkills
    df = df[df['team_att'] != df['team_vic']]
    df.reindex()
    return df


def plot_heat(x, y, image, title):
    import matplotlib.pyplot as plt

    fig, ax = plt.subplots()
    ax.imshow(image)
    ax.scatter(x, y)
    fig.savefig(title + '.png')


def get_crop(gpo_lines):
    import numpy as np

    from fh2parse.map import bounding_rectangle
    from fh2parse.parse import parse_combatareas

    areas = parse_combatareas(gpo_lines)
    del areas['time_to_pain']
    if 'damage' in areas:
        del areas['damage']
    all_points = []
    for _, vals in areas.items():
        points = vals['points']
        all_points.extend(points)
    all_points = np.array(all_points)
    xmin, ymin, xmax, ymax = bounding_rectangle(all_points)
    return xmin, ymin, xmax, ymax


def main(fname, mapname, layer):
    from os.path import join

    import numpy as np

    from fh2parse import LEVEL_DIR
    from fh2parse.map import MiniMap
    from fh2parse.parse import parse_heightmap_size, read_confile

    mapdir = join(LEVEL_DIR, mapname)
    ddsname = join(mapdir, 'hud', 'minimap', 'ingamemap.dds')
    heightdata_con = join(mapdir, 'heightdata.con')

    map_size = parse_heightmap_size(read_confile(heightdata_con))
    mini = MiniMap(ddsname, size=map_size)
    print(map_size)
    print(mini.img.size)
    print(mini.m2i_scale)
    gpo_con = join(
        mapdir, 'gamemodes', 'gpm_cq', str(layer), 'gameplayobjects.con'
    )
    gpo_lines = read_confile(gpo_con, strip_empty_lines=False)
    xmin, xmax, ymin, ymax = get_crop(gpo_lines)
    # mini.crop(xmin, xmax, ymin, ymax)

    df = read_json(fname, layer)
    df = filter_df(df)
    queries = {
        'allies_frags': 'team_att == 2',
        'axis_frags': 'team_att == 1',
    }
    x_vic_pil, z_vic_pil = mini.transform_coord(
        np.atleast_1d(df['pos_vic_x']),
        np.atleast_1d(df['pos_vic_z']),
    )
    df['pos_vic_pil_x'] = x_vic_pil
    df['pos_vic_pil_z'] = z_vic_pil
    print('----')
    print('    ')
    print('----')
    print(df['pos_vic_z'].describe())
    print('----')
    print(df['pos_vic_pil_z'].describe())
    print('----')
    print('    ')
    print('----')
    print(df['pos_vic_x'].describe())
    print('----')
    print(df['pos_vic_pil_x'].describe())
    print(xmin, xmax, ymin, ymax)

    for qname, query in queries.items():
        sub_df = df.query(query)
        x_pil = np.atleast_1d(sub_df['pos_vic_pil_x'])
        y_pil = np.atleast_1d(sub_df['pos_vic_pil_z'])
        plot_heat(x_pil, y_pil, title=mapname + '_' + qname, image=mini.img)


if __name__ == '__main__':
    from docopt import docopt
    args = docopt(__doc__)
    logfile = args['LOGFILE']
    mapname = args['MAPNAME']
    layer = int(args['LAYER'])
    main(logfile, mapname, layer)
