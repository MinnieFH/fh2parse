#!/usr/bin/env python
# pylint: disable=W0232,C0103,C0111
# vim:set ts=4 sts=4 sw=4 et syntax=python:
"""
List unique GamePlayObjects from a map

Usage:
    list_gpo.py [options] MAPNAME

Options:
    -v --verbose                Print more output
    -g <gm>, --gamemode <gm>    Gamemode [Default: 64]
"""


def list_gpo(mapname, gamemode=64, verbose=False):
    """List GamePlayObjects from map (vehicles, kits, static guns)."""
    from fh2parse import LEVEL_DIR, PAGE_DIR
    from fh2parse.parse import (
        read_confile,
        parse_object_spawns,
    )

    from os.path import join, exists
    from os import sep

    mapdir = join(LEVEL_DIR, mapname)
    if verbose:
        print(mapdir)

    gpo_con = join(
        mapdir, 'gamemodes', 'gpm_cq', str(gamemode), 'gameplayobjects.con'
    )
    if not exists(gpo_con):
        "{} does not exist, aborting...".format(gpo_con)
        return

    lines = read_confile(gpo_con, strip_empty_lines=False)
    spawned_objs = parse_object_spawns(lines)

    if verbose:
        print(spawned_objs)
    object_names = {s['object_name'] for s in spawned_objs}
    if verbose:
        print("GamePlayObjects in {}".format(mapname))
    print('\n'.join(sorted(object_names)))


if __name__ == '__main__':
    from docopt import docopt
    args = docopt(__doc__)
    mapname = args['MAPNAME']
    gamemode = int(args['--gamemode'])
    verbose = args['--verbose']
    list_gpo(mapname, gamemode=gamemode, verbose=verbose)
