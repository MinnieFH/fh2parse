#!/usr/bin/env bash

# change this to your installation location
GAMEDIR=~/.wine/drive_c/fh2/mods/fh2/levels

help="
Usage:
  unpack_map.sh [options] LEVEL

Options:
  -h, --help    Show help
  --verbose     Show more messages
"
eval "$(docopts -A args -h "$help" : "$@")"

LEVEL="${args['LEVEL']}"

EXTDIR=../data/levels

if [ ! -f "$GAMEDIR/$LEVEL/server.zip" ]; then
  echo "$GAMEDIR/$LEVEL" " does not have a 'server.zip', aborting..."
  exit
fi
if [ ! -f "$GAMEDIR/$LEVEL/client.zip" ]; then
  echo "$GAMEDIR/$LEVEL" " does not have a 'client.zip', aborting..."
  exit
fi

if [ -d "$EXTDIR/$LEVEL" ]; then
  echo "directory " "$EXTDIR/$LEVEL" " exists, skipping..."
  #echo "directory " "$EXTDIR/$LEVEL" " exists, removing..."
  exit
  #rm -rf "$EXTDIR/$LEVEL"
fi

echo -n "unpacking map'" "$LEVEL" "'... "
mkdir -p "$EXTDIR/$LEVEL"
unzip -o -q -d "$EXTDIR/$LEVEL" "$GAMEDIR/$LEVEL/server.zip"
unzip -o -q -d "$EXTDIR/$LEVEL" "$GAMEDIR/$LEVEL/client.zip"

# rename to lowercase, because case is inconsistent across maps
# goddamn ms windows
find "$EXTDIR" -depth -exec perl-rename 's/(.*)\/([^\/]*)/$1\/\L$2/' {} \;
echo "OK"
