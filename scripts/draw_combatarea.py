#!/usr/bin/env python
# pylint: disable=W0232,C0103,C0111,F0401
# vim:set ts=4 sts=4 sw=4 et syntax=python:
"""
Draw combatareas of a map

Usage:
    draw_combatarea.py [options] GPOFILE MINIMAP HEIGHTDATA

Options:
    -h, --help                  Show this help
    -v, --verbose               Print more output
    -o <f>, --outfile <f>       Output file with the drawing
    -p <pr>, --prefix <pr>      Prefix for outfiles
"""


def draw_combatarea(gpo_fname, map_fname, heightdata_con, prefix=None):
    import numpy as np

    from fh2parse.map import MiniMap, bounding_rectangle
    from fh2parse.parse import (
        read_confile, parse_combatareas, parse_heightmap_size
    )

    if not prefix:
        prefix = ''
    else:
        prefix = prefix + '_'

    con_lines = read_confile(gpo_fname)
    areas = parse_combatareas(con_lines)
    del areas['time_to_pain']
    if 'damage' in areas:
        del areas['damage']

    map_size = 2048  # ogledow
    map_size = parse_heightmap_size(read_confile(heightdata_con))

    colors = [
        (255, 0, 0),
        (0, 255, 0),
        (0, 0, 255),
        (0, 0, 0),
        (128, 128, 128),
        (255, 255, 255),
        (255, 255, 0),
        (0, 255, 255),
        (255, 0, 255),
    ]
    i_axis = 0
    i_allies = 0

    all_points = []
    maps_ax = []
    maps_al = []

    for areaname, vals in areas.items():
        # print(areaname)
        # print(vals)
        points = vals['points']
        all_points.extend(points)
        if 'axis' in areaname.lower():
            col = colors[i_axis]
            mini_axis = MiniMap(map_fname, size=map_size)
            mini_axis.add_polygon(points, color=col)
            maps_ax.append(mini_axis)
            i_axis += 1
        if 'allies' in areaname.lower():
            col = colors[i_allies]
            mini_allies = MiniMap(map_fname, size=map_size)
            mini_allies.add_polygon(points, color=col)
            maps_al.append(mini_allies)
            i_allies += 1

    all_points = np.array(all_points)
    xmin, ymin, xmax, ymax = bounding_rectangle(all_points)

    for i, mm in enumerate(maps_al):
        mm.crop(xmin, xmax, ymin, ymax)
        mm.draw_map('{}areas_allies_{}.webp'.format(prefix, i))
    for i, mm in enumerate(maps_ax):
        mm.draw_map('{}areas_axis_{}.webp'.format(prefix, i))
        mm.crop(xmin, xmax, ymin, ymax)


if __name__ == '__main__':
    from docopt import docopt
    args = docopt(__doc__)
    verbose_ = args['--verbose']
    gpofile = args['GPOFILE']
    minimap = args['MINIMAP']
    heightdata = args['HEIGHTDATA']
    prefix = args['--prefix']
    draw_combatarea(gpofile, minimap, prefix=prefix, heightdata_con=heightdata)
