#!/usr/bin/env bash
help="
Usage:
  mkd_to_html.sh [options] MDFILE

Options:
  -h, --help                Show help
  --verbose                 Show more messages
"
eval "$(docopts -A args -h "$help" : "$@")"

src="${args['MDFILE']}"
out=${src%.md}.html

STYLE=style.css

pandoc --from markdown+smart+link_attributes+multiline_tables+simple_tables+pipe_tables+definition_lists+backtick_code_blocks+fenced_code_attributes+implicit_figures+tex_math_dollars+header_attributes \
	--standalone  --katex \
	-t html5 --css="$STYLE" "$src" -o "$out"

# make html compatible to vimb
sed -i '/xmlns/c\<html>' "$out"
