#!/usr/bin/env python
# pylint: disable=W0232,C0103,C0111
# vim:set ts=4 sts=4 sw=4 et syntax=python:
"""
Grab ammoboxes from a map file and draw them onto the minimap

Usage:
    draw_ammo.py [options] MAPNAME

Options:
    -v --verbose                Print more output
    -o <dir>, --outdir <dir>    Where to save the image. Defaults to the `page`
                                directory above
"""


def draw_ammo(mapname, outdir, verbose=False):
    """Draw Minimap with ammocrates"""
    from os.path import join, exists
    from os import sep

    from fh2parse import LEVEL_DIR, PAGE_DIR
    from fh2parse.parse import read_confile, parse_heightmap_size, parse_ammo
    from fh2parse.map import MiniMap

    mapdir = join(LEVEL_DIR, mapname)
    if verbose:
        print(mapdir)
    static_con = join(mapdir, 'staticobjects.con')
    heightdata_con = join(mapdir, 'heightdata.con')
    ddsname = join(mapdir, 'hud', 'minimap', 'ingamemap.dds')
    if verbose:
        print(static_con)
    assert exists(static_con), "{} does not exist".format(static_con)
    assert exists(heightdata_con), "{} does not exist".format(heightdata_con)
    assert exists(ddsname), "{} does not exist".format(ddsname)

    if not outdir:
        if verbose:
            print('Setting outdir to ', PAGE_DIR)
        outdir = PAGE_DIR

    image_name = mapname.rstrip(sep) + '_ammo.webp'
    outfile = join(outdir, image_name)

    map_size = parse_heightmap_size(read_confile(heightdata_con))
    crates = parse_ammo(read_confile(static_con))

    if verbose:
        import numpy as np
        crates_arr = np.array(crates)
        print(crates_arr)
        print(crates_arr.shape)

    # gotta take size from the heightmap
    mini = MiniMap(ddsname, size=map_size)
    if verbose:
        print(map_size)
        print(mini.img.size)
    #mini.add_marker((0, 0, 0), marker_color=(0, 255, 255))
    #mini.add_marker((50, 0, 50), marker_color=(0, 255, 255))
    #mini.add_marker((100, 0, 100), marker_color=(0, 255, 255))
    #mini.add_marker((150, 0, 150), marker_color=(0, 255, 255))
    #mini.add_marker((200, 0, 200), marker_color=(0, 255, 255))
    #mini.add_marker((250, 0, 250), marker_color=(0, 255, 255))
    #mini.add_marker((300, 0, 300), marker_color=(0, 255, 255))
    for c in crates:
        mini.add_marker(c)
    if verbose:
        print('writing to ', outfile)
    mini.draw_map(outfile)


if __name__ == '__main__':
    from docopt import docopt
    args = docopt(__doc__)
    mapname = args['MAPNAME']
    outdir = args['--outdir']
    verbose = args['--verbose']
    draw_ammo(mapname, outdir, verbose=verbose)
