#!/usr/bin/env bash

build_page() {
  LEVEL="$1"

  PAGEDIR=../data/levels

  EXTDIR=../data/levels
  if [ ! -d "$EXTDIR/$LEVEL/" ]; then
    echo "$EXTDIR/$LEVEL" " folder does not found, trying to extract mapfiles"
    ./unpack_map.sh "$LEVEL"
  fi

  ./draw_ammo.py "$LEVEL"
  ./mdpage.py "$LEVEL"

}

build_page "$1"
