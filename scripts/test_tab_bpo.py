#!/usr/bin/env python
# pylint: disable=W0232,C0103,C0111
# vim:set ts=4 sts=4 sw=4 et syntax=python:
"""
List unique GamePlayObjects from a map

Usage:
    tab_gpo.py [options] MAPNAME

Options:
    -v --verbose                Print more output
    -g <gm>, --gamemode <gm>    Gamemode [Default: 64]
"""
ALLCOLS = [
    'gpo_subcat',
    'gpo_cat',
    'gpo_name',
    'pos_x',
    'pos_y',
    'pos_z',
    'flag',
    'is_locked',
    'team',
    'instance',
    'gpo_cat_disp',
    'gpo_subcat_disp',
]


def list_gpo(mapname, gamemode=64, verbose=False):
    """List GamePlayObjects from map (vehicles, kits, static guns)."""

    from fh2parse import LEVEL_DIR, PAGE_DIR, GPO_CSV
    from fh2parse.gpo import GPOTable
    from fh2parse.parse import (
        read_confile,
        parse_object_spawns,
        parse_ammo,
        ammo_as_gpo,
    )

    from os.path import join, exists
    from os import sep

    mapdir = join(LEVEL_DIR, mapname)
    if verbose:
        print(mapdir)

    gpo_con = join(
        mapdir, 'gamemodes', 'gpm_cq', str(gamemode), 'gameplayobjects.con'
    )
    if not exists(gpo_con):
        "{} does not exist, aborting...".format(gpo_con)
        return
    static_con = join(mapdir, 'staticobjects.con')
    if not exists(static_con):
        "{} does not exist, aborting...".format(static_con)
        return

    gpo_lines = read_confile(gpo_con, strip_empty_lines=False)
    spawned_objs = parse_object_spawns(gpo_lines)
    if verbose:
        print(spawned_objs)
    ammo_crates = parse_ammo(read_confile(static_con))
    spawned_objs.extend(ammo_as_gpo(ammo_crates))

    gpo_tab = GPOTable(GPO_CSV)
    objs_with_cats = [gpo_tab.amend(ob) for ob in spawned_objs]
    if verbose:
        print(objs_with_cats)
    return objs_with_cats


def dump_table(objs, tabname, cols=ALLCOLS):
    import pandas as pd
    df = pd.DataFrame.from_dict(objs)
    df = df[cols]
    df = df.sort_values(by='gpo_subcat')
    with open(tabname, 'w') as f:
        f.write(df.to_markdown(showindex=False))


if __name__ == '__main__':
    from docopt import docopt
    from pprint import pprint
    args = docopt(__doc__)
    mapname = args['MAPNAME']
    gamemode = int(args['--gamemode'])
    verbose = args['--verbose']
    objs = list_gpo(mapname, gamemode=gamemode, verbose=verbose)
    tabname = mapname + '.md'
    pprint(objs)
    dump_table(
        objs,
        tabname,
    )
