#!/usr/bin/env bash

conv_mapdesc() {

# the file is in utf-16 and has escape characters, ascii 27 = 0x1B Hex

  iconv -f UTF-16LE -t ascii//translit maps_fh.utxt > maps_fh.buf.txt
  tr '\0-\10\13\14\16-\37' '[ *]' < maps_fh.buf.txt > maps_fh.txt
  rm maps_fh.buf.txt
  tr '\0-\10\13\14\16-\37' '[ *]' < maps_fh.txt > maps_fh.trim.txt
}

conv_mapdesc
