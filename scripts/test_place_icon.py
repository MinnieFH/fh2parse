#!/usr/bin/env python
# pylint: disable=W0232,C0103,C0111
# vim:set ts=4 sts=4 sw=4 et syntax=python:
"""
Usage:
    place_icon.py MAPFILE ICONFILE
"""

if __name__ == '__main__':
    from docopt import docopt
    args = docopt(__doc__)
    mapfile = args['MAPFILE']
    iconfile = args['ICONFILE']

    from PIL import Image
    import numpy as np

    from fh2parse.map import recolor

    canvas = Image.open(mapfile)
    ico = Image.open(iconfile)
    ico = ico.convert('RGBA')

    yellow = (255, 255, 0)
    blue = (0, 0, 255)
    white = (255, 255, 255)

    ico_y = recolor(ico, new_color=yellow, old_color=white)
    ico_b = recolor(ico, new_color=blue, old_color=white)

    canvas.paste(ico, (100, 100))
    canvas.paste(ico_y, (200, 200))
    canvas.paste(ico_y, (300, 300))
    canvas.save('test.webp')
