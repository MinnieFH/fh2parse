#!/usr/bin/env python
# pylint: disable=W0232,C0103,C0111
# vim:set ts=4 sts=4 sw=4 et syntax=python:
"""
Generate markdown page

Usage:
    mdpage.py [options] MAPNAME

Options:
    -v --verbose                Print more output
    -o <dir>, --outdir <dir>    Output directory. Defaults to the `page`
                                directory above
"""


def mdpage(mapname, outdir, verbose=False):
    """Build markdown page from map"""

    from os.path import join

    from fh2parse import PAGE_DIR
    from fh2parse.wiki import mkd_page

    if not outdir:
        if verbose:
            print('Setting outdir to ', PAGE_DIR)
        outdir = PAGE_DIR

    mkd = mkd_page(mapname)
    outfile = join(outdir, '{}.md'.format(mapname))

    if verbose:
        print("Writing '{}' page to '{}'...".format(mapname, outfile))
    with open(outfile, 'w') as f:
        f.write(mkd)


if __name__ == '__main__':
    from docopt import docopt
    args = docopt(__doc__)
    mapname = args['MAPNAME']
    outdir = args['--outdir']
    verbose = args['--verbose']
    mdpage(mapname, outdir, verbose=verbose)
