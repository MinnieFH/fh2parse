#!/usr/bin/env bash

set -e

build_all_pages() {
  while read l; 
  do 
    for ly in 16 32 64
    do
      ./build_page.py -l "$ly" "$l" 
    done
  done < ../data/mapnames.txt
}

build_all_pages
