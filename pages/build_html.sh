#!/usr/bin/env bash

for k in *.md; do 
  echo "$k"
  ../scripts/mkd_to_html.sh "$k" ; 
done
