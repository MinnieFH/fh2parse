# fh2parse

> I have to say, although it'd be an absolute ton of work, a wiki that
> details vehicle/pickup kit/stationary/ammobox locations 
> for each map would be amazing.

`fh2parse` is intended to address this idea, by autogenerating webpages 
listing kits and showing them on a minimap, all from the FH2 level files, 
with minimal manual input for easy maintenance.

## Install

You need a working python3 env. Install the required packages with
```shell
pip install -Ur requirements.txt
```

The working scripts are in the `scripts` directory. You will need to
change the locations in the shell scripts (and in `fh2parse/__init__.py`)
to make them point to your FH2 installation. I use linux + wine and mine is 
installed at `~/.wine/drive_c/fh2/`

To run the shell scripts in linux (and maybe on Mac OS?), you need a Go
environment and install the [docopts](https://github.com/docopt/docopts)
package, for command line args parsing in bash. Also I write webpages in
markdown, and for conversion to HTML I use [pandoc](https://pandoc.org/)
(which is sadly in Haskell...)

## Features

done:

- beautiful map views online at [minniefh.bitbucket.io](https://minniefh.bitbucket.io)


todo:

- make more and better custom icons 
- include more categories (e.g. split up tanks into light, heavy, TD, ...)
- crop the minimap for each layer to the actually ingame-visible area
