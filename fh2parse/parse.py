# pylint: disable=W0232,C0103,C0111
# vim:set ts=4 sts=4 sw=4 et syntax=python:
"""
Simple parser for ingame objects.

Remember that they use weird videogame cartesian coordinates, where
height-above-ground is called `y`. Top left of minimap is
"""

from fh2parse import DESCRIPTION_LIST_FILE


def read_confile(filename, strip_empty_lines=False, encoding=None):
    """Read a .con file and do some basic filtering and cleaning.

    Strips leading whitespace, filters out comment blocks, and
    ignores if clauses (assumes true, since usually it checks for stuff like
    "are we on a server" (yes, because we want to read server objects).

    Parameters
    ----------
    filename: str
        name of a .con file to parse
    strip_empty_lines: bool
        Remove blank lines from file? Some .con files need them to delimit
        blocks
    encoding:
        Encoding argument passed through to builtin `open`

    Returns
    -------
    lines: list(str)
        The filtered lines from the confile.
    """
    lines = []
    confile = open(filename, encoding=encoding, errors='ignore')
    in_ignore_mode = False
    for line in confile.readlines():
        line = line.lstrip()
        words = line.split()
        if not words and strip_empty_lines:
            # empty line
            continue
        if not words:
            lines.append(line)
            continue
        firstword = words[0].lower()
        if firstword in ('rem', 'if', 'endif'):
            # if comment, continue
            # for 'if', assume always true, so dont go ignore mode
            continue
        if firstword == 'beginrem':
            # .con comment block start
            in_ignore_mode = True
            continue
        if firstword == 'endrem':
            # .con comment block end
            in_ignore_mode = False
        if in_ignore_mode:
            # recently met start of comment block, didnt meet end yet
            # (checked before)
            continue
        lines.append(line)
    confile.close()
    return lines


def read_description(mapname, map_desc_file=DESCRIPTION_LIST_FILE):
    """Grab nice readable name + description for a map.

    Parameters
    ----------
    mapname: name of the maps *folder*

    Returns
    -------
    passthrough from parse_description
    """
    # encoding to deal with weird unicode chars (omit the BOM)
    lines = read_confile(map_desc_file, encoding='utf-8-sig')
    return parse_description(lines, mapname)


def parse_description(lines, mapname):
    """Grab nice readable name + description for a map.

    Parameters
    ----------
    lines: list(str)
        Lines from description file, filtered. Probably read with
        `'utf-8-sig'` encoding.

    Returns
    -------
    display_name: str
        Long human-readable name
    description: str
        Long map description from loading screen
    """
    map_desc = ''
    map_display_name = ''
    is_in_description_block = False
    # encoding to deal with weird unicode chars (omit the BOM)
    for line in lines:
        line = line.lstrip().rstrip()
        words = line.split()
        if ((not words or line.isspace()) and not is_in_description_block):
            # empty line not following description block
            continue
        if ((not words or line.isspace()) and is_in_description_block):
            # after description block: empty line
            is_in_description_block = False
            continue
        if line.startswith('GAMEMODE'):
            # ignore this. might wander into the description block (???)
            continue
        if (is_in_description_block and line.startswith('CP')):
            # after description block: CP stuff
            # blocks don't always end with blank line
            is_in_description_block = False
            continue
        if (
            words[0].lower() == 'loadingscreen_mapdescription_{}'.format(
                mapname.lower()
            )
        ):
            # start of mapdesc block
            is_in_description_block = True
            map_desc = ' '.join(words[1:])
            continue
        if is_in_description_block:
            # continuation of mapdesc block
            map_desc = '\n'.join((map_desc, line))
            continue
        if 'hud_levelname_{}'.format(mapname.lower()
                                     ) == line.split()[0].lower():
            # not checking for equality, there are some invisible unicode
            # chars at the beginning of some strings???
            map_display_name = ' '.join(line.split()[1:])
            continue

    if not map_desc:
        raise ValueError('No map description found!')
    if not map_display_name:
        raise ValueError('No map display name found!')
    return map_display_name, map_desc


def unpack_coordinate(coord_string):
    """Parse a coordinate string from a .con file.

    Parameters
    ----------
    coord_string: str
        This looks like "-345.073/148.429/389.721"

    Returns
    -------
    x, y, z: tuple of double coords

    """
    x, y, z = (float(k) for k in coord_string.split('/'))
    return x, y, z


def parse_heightmap_size(lines):
    """Parse a Heightdata.con, looking for the heightmap size.

    Look for the first instance of something like
    "heightmapcluster.setHeightmapSize 2048"

    Parameters
    ----------
    lines: list(str)
        Lines of the Heightdata.con, already filtered
    Returns
    -------
    mapsize: int
        Size of the heightmap, `2^X` (e.g. 1024)
    """
    mapsize = 0
    for line in lines:
        words = line.split()
        if 'heightmapcluster.setHeightmapSize' in words:
            mapsize = int(words[1])
            return mapsize
        continue
    if not mapsize:
        raise ValueError('Could not find a heightmap size!')
    return mapsize


def parse_ammo(lines, debug=False):
    """Parse a StaticObjects.con, looking for ammo crate locations.

    Parameters
    ----------
    lines: list(str)
        Lines of the Staticobjects.con, already filtered
    debug: bool
        whether to print more stuff

    Returns
    -------
    ammocrates: list([float, float, float, ])
        List of coordinate tuples holding ammocrate locations.

    """
    ammobox_names = {
        'fh_ammocrate',
        'x_player_ammobox_closed',
        'x_player_ammobox_open',
        'pco_player_ammobox_closed',
        'pco_player_ammobox_open',
        'player_ammobox_cls',
        'player_ammobox',
    }
    in_interesting_block = False
    ammocrates = []
    for line in lines:
        words = line.split()
        if not words:
            continue
        if debug:
            print(words)
        for nam in ammobox_names:
            if nam in words:
                # look for stuff to parse in the next lines
                in_interesting_block = True
                continue
        # block looks like "Object.absolutePosition -345.073/148.429/389.721"
        if in_interesting_block and words[0] == 'Object.absolutePosition':
            x, y, z = unpack_coordinate(words[1])
            ammocrates.append((
                x,
                y,
                z,
            ))
            # done grabbing all we wanted
            in_interesting_block = False
    return ammocrates


def ammo_as_gpo(ammo_crates):
    """Pack ammocrates into GPO structure.

    Parameters
    ----------
    ammo_crates: list(pos)
        List of ammo crate positions, which are 3-tuple of float.

    Returns
    -------
    ammo_gpo: list(dict)
        Ammocrates with GPO-style padding.
    """
    out = []
    for i, c in enumerate(ammo_crates):
        d = {}
        d['instance'] = 'ammo_crate_{}'.format(i)
        d['gpo_name'] = 'ammo_crate'
        d['is_locked'] = False
        d['team'] = 0
        d['pos_x'] = c[0]
        d['pos_y'] = c[1]
        d['pos_z'] = c[2]
        d['flag'] = 0
        out.append(d)
    return out


def parse_object_spawns(lines):
    """Parse a GamePlayObjects.con, looking for spawned objects.

    Parameters
    ----------
    lines: list(str)
        Lines of the GamePlayObjects.con, already filtered.
        MUST have blank lines still there.
    """
    instances = [
        t for t in parse_object_instances(lines)
        if t['gpo_type'] == 'ObjectSpawner'
    ]
    spawners = parse_object_spawners(lines)

    spawned_objs = []
    for i in instances:
        inst = i['instance']
        objname = i['gpo_name']
        is_lock = i['is_locked']
        team = i['team']
        for s in spawners:
            if s['instance'] == inst:
                d = {}
                d['instance'] = inst
                d['gpo_name'] = objname
                d['is_locked'] = is_lock
                d['team'] = team
                d['pos_x'] = s['pos_x']
                d['pos_y'] = s['pos_y']
                d['pos_z'] = s['pos_z']
                d['flag'] = s['flag']
                spawned_objs.append(d)
    return spawned_objs


def parse_controlpoints(lines):
    """Parse a GamePlayObjects.con, looking for ControlPoints.

    Parameters
    ----------
    lines: list(str)
        Lines of the GamePlayObjects.con, already filtered.
        MUST have blank lines still there.
    """
    return [
        t for t in parse_object_instances(lines)
        if t['gpo_type'] == 'ControlPoint'
    ]


def parse_spawnpoints(lines):
    """Parse a GamePlayObjects.con, looking for SpawnPoints.

    Parameters
    ----------
    lines: list(str)
        Lines of the GamePlayObjects.con, already filtered.
        MUST have blank lines still there.
    """
    return [
        t for t in parse_object_instances(lines)
        if t['gpo_type'] == 'SpawnPoint'
    ]


def parse_object_instances(lines):
    """Parse a GamePlayObjects.con, looking for object instances.

    Parameters
    ----------
    lines: list(str)
        Lines of the GamePlayObjects.con, already filtered.
        MUST have blank lines still there.

    Returns
    -------
    instances: list(dict)
    """
    instances = []
    in_interesting_block = False
    # defaults if missing value
    is_locked = False
    team_id = 0
    objname = ''
    objtype = ''
    instname = ''

    for line in lines:
        # relying on the entries being ordered:
        # instname
        # objname
        # team (might not be there)
        # islocked (might not be there)
        # <endblock on empty line>
        words = line.split()
        if not words:
            # empty line, end of block
            # reset to default
            if instname:
                # don't write empty, on newline
                # team_id 0 = both teams, 1 = axis, 2 = allies
                instances.append(
                    {
                        'instance': instname,
                        'gpo_name': objname,
                        'gpo_type': objtype,
                        'is_locked': is_locked,
                        'team': team_id,
                    }
                )
            objname = ''
            objtype = ''
            instname = ''
            is_locked = False
            team_id = 0
            in_interesting_block = False
            continue
        if words[0] == 'ObjectTemplate.create':
            instname = words[2]
            objtype = words[1]
            in_interesting_block = True
            continue
        if not in_interesting_block:
            continue
        if words[0] == 'ObjectTemplate.teamOnVehicle':
            is_locked = bool(words[1])
            continue
        if words[0] == 'ObjectTemplate.team':
            team_id = int(words[1])
            continue
        if words[0] == 'ObjectTemplate.setObjectTemplate':
            objname = words[2]
            continue
        if not line:
            # empty line, end of block
            # reset to default
            instances.append((instname, objname, objtype, is_locked, team_id))
            objname = ''
            objtype = ''
            instname = ''
            is_locked = False
            team_id = 0
            in_interesting_block = False
            continue
    return instances


def parse_object_spawners(lines):
    """Parse a GamePlayObjects.con, looking for object spawners.

    Parameters
    ----------
    lines: list(str)
        Lines of the GamePlayObjects.con, already filtered

    Returns
    -------
    spawners: list(dict)
    """
    spawners = []
    in_interesting_block = False
    # defaults if missing value
    instname = ''
    pos = (0, 0, 0)
    flag_id = 0

    for line in lines:
        # relying on the entries being ordered:
        # instname
        # objname (might not be there)
        # flag_id (might not be there)
        # <endblock on empty line>
        words = line.split()
        if not words:
            # empty line, end of block
            # reset to default
            if instname:
                # don't write empty, on newline
                spawners.append(
                    {
                        'instance': instname,
                        'pos_x': pos[0],
                        'pos_y': pos[1],
                        'pos_z': pos[2],
                        'flag': flag_id,
                    }
                )
            instname = ''
            pos = (0, 0, 0)
            flag_id = 0
            in_interesting_block = False
            continue
        if words[0] == 'Object.create':
            instname = words[1]
            in_interesting_block = True
            continue
        if not in_interesting_block:
            continue
        if words[0] == 'Object.absolutePosition':
            pos = unpack_coordinate(words[1])
            continue
        if words[0] == 'Object.setControlPointId':
            flag_id = int(words[1])
            continue
    return spawners


def parse_combatareas(lines):
    """Parse a GamePlayObjects.con, looking for CombatAreas.

    Parameters
    ----------
    lines: list(str)
        Lines of the GamePlayObjects.con, already filtered.

    Returns
    -------
    out: dict
        dict, where the keys are each area names, with attributes as subdicts
    """
    out = {}
    current_area = ''
    for line in lines:
        if not line:
            continue
        if not line.startswith('CombatArea'):
            continue
        if line.split()[0] in (
            'CombatAreaManager.use',
            'CombatArea.min',
            'CombatArea.max',
        ):
            continue
        if line.startswith('CombatAreaManager.timeAllowedOutside'):
            t_pain = float(line.split()[-1])
            out['time_to_pain'] = t_pain
            continue
        if line.startswith('CombatAreaManager.damage'):
            dmg = float(line.split()[-1])
            out['damage'] = dmg
            continue
        if line.startswith('CombatArea.create'):
            ar = line.split()[-1]
            current_area = ar
            out[current_area] = {}
            out[current_area]['points'] = []
            continue
        if line.startswith('CombatArea.team'):
            tm = int(line.split()[-1])
            out[current_area]['team'] = tm
            continue
        if line.startswith('CombatArea.vehicles'):
            vh = int(line.split()[-1])
            out[current_area]['vehicles'] = vh
            continue
        if line.startswith('CombatArea.inverted'):
            inv = line.split()[-1]
            out[current_area]['inverted'] = inv
            continue
        if line.startswith('CombatArea.layer'):
            ly = int(line.split()[-1])
            out[current_area]['layer'] = ly
            continue
        if line.startswith('CombatArea.addAreaPoint'):
            coords = line.split()[-1]
            x, y = coords.split('/')
            x = float(x)
            y = float(y)
            out[current_area]['points'].append((x, y))
            continue
    return out
