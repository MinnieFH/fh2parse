# pylint: disable=W0232,C0103,C0111
# vim:set ts=4 sts=4 sw=4 et syntax=python:
"""Lookup table for GPO's: Is it a vehicle or kit, and what kind?
"""
import csv
from copy import deepcopy

OBJECT_CATEGORIES = {
    'ammo_crate': {'ammo_crate', },
    'flag': {'flag', },
    'spawn': {'spawn', },
    'noidea': {'noidea', },
    'kit':
        {
            'ammo',
            'antitank',
            'arty_dep',
            'assault',
            'commando',
            'easteregg',
            'engineer',
            'mg',
            'mg_dep',
            'sniper',
            'medic',
            'parachute',
            'flame',
            'zooka',
            'at_rifle',
        },
    'static': {
        'arty',
        'pak',
        'mg_nest',
        'flak',
        'radio',
    },
    'vehicle':
        {
            'car',
            'apc',
            'tank',
            'plane',
            'ship',
            'arty_sp',
            'flak_sp',
            'pak_sp',
            'supply',
            'civilian',
            'recon',
        }
}

DISPLAY_NAMES = {
    # categories
    'spawn': 'Spawn point',
    'flag': 'Control Point',
    'noidea': 'FIXME UNASSIGNED',
    'ammo_crate': 'Static Ammo Crate',
    'kit': 'Pickup Kit',
    'static': 'Static Emplacement',
    'vehicle': 'Vehicle',
    # kits
    'ammo': 'Ammo Kit',
    'antitank': 'Tankhunter Kit',
    'arty_dep': 'Deployable Arty',
    'assault': 'Assault Kit',
    'commando': 'Commando Kit',
    'easteregg': 'Easteregg',
    'engineer': 'Engineer Kit',
    'mg': 'MG Kit',
    'mg_dep': 'Deployable MG',
    'sniper': 'Sniper Kit',
    'medic': 'Medic Kit',
    'parachute': 'Parachute Kit',
    'flame': 'Flamethrower Kit',
    'zooka': 'HEAT Thrower',
    'at_rifle': 'AT Rifle',
    # statics
    'arty': 'Artillery',
    'pak': 'Anti-tank Gun',
    'mg_nest': 'Static MG',
    'flak': 'Anti-aircraft Gun',
    'radio': 'Radio',
    # vehicles
    'car': 'Car',
    'apc': 'APC',
    'tank': 'Tank',
    'plane': 'Airplane',
    'ship': 'Ship',
    'arty_sp': 'Mobile Arty',
    'flak_sp': 'Mobile FlaK',
    'pak_sp': 'Mobile PaK',
    'supply': 'Supply Vehicle',
    'civilian': 'Civilian Vehicle',
    'recon': 'Scout Vehicle',
}
SUBCAT_ICONS = {
    # parent cats
    'spawn': 'todo.webp',
    'flag': 'todo.webp',
    'noidea': 'todo.webp',
    'ammo_crate': 'ammo.webp',
    # kits
    'ammo': 'ammo.webp',
    'antitank': 'boom.webp',
    'arty_dep': 'arty_dep.webp',
    'assault': 'assault.webp',
    'commando': 'sword.webp',
    'easteregg': 'easteregg.webp',
    'engineer': 'engineer.webp',
    'mg': 'todo.webp',
    'mg_dep': 'mg_tripod.webp',
    'sniper': 'sniper.webp',
    'medic': 'medic.webp',
    'parachute': 'para.webp',
    'flame': 'flame.webp',
    'zooka': 'rocket.webp',
    'at_rifle': 'todo.webp',
    # static guns
    'arty': 'arty.webp',
    'pak': 'pak.webp',
    'mg_nest': 'mg_tripod.webp',
    'flak': 'flak.webp',
    'radio': 'todo.webp',
    # vehicles
    'car': 'car.webp',
    'apc': 'apc.webp',
    'tank': 'tank.webp',
    'plane': 'plane.webp',
    'ship': 'boat.webp',
    'arty_sp': 'arty.webp',
    'pak_sp': 'pak.webp',
    'flak_sp': 'flak.webp',
    'supply': 'ammo.webp',
    'civilian': 'car.webp',
    'recon': 'binoc.webp',
}

FORBIDDEN_ITEMS = {
    'autogyro',
}


class GPOTable():
    """Lookup table for GamePlayObjects.

    Parameters
    ----------
    filename: str
        Filename of the CSV file holding the subcategories for each GPO

    Methods
    -------
    get_gategories(objname)
        Get category + subcategory for each kit (e.g. 'kit', 'sniper')

    Attributes
    ----------
    obj_cats: dict(cat: set(subcats))
    """
    obj_cats = OBJECT_CATEGORIES

    def __init__(self, filename):
        self.known_gpos = {}
        with open(filename) as f:
            reader = csv.DictReader(f, skipinitialspace=True, delimiter=',')
            for row in reader:
                gponame = row['OBJNAME'].lower()
                gposubcat = row['OBJSUBCAT'].lower()
                gpocat = self._get_parent_category(gposubcat).lower()
                self.known_gpos[gponame] = {
                    'gpo_cat': gpocat,
                    'gpo_subcat': gposubcat,
                }

    def get_cats(self, objname):
        """Get category of a GPO.

        Everything is treated as lowercase, by the way, so be wary of kits
        which differ only in cApItAlIzAtIoN.

        Parameters
        ----------
        objname: str
            name of the object (e.g. 'zis3_static')

        Returns
        -------
        cat, subcat: (str, str)
            Category and subcategory names (e.g. ('static', 'pak'))
        """

        objname = objname.lower()
        if objname not in self.known_gpos:
            cat = 'noidea'
            subcat = 'noidea'
        else:
            cat = self.known_gpos[objname]['gpo_cat']
            subcat = self.known_gpos[objname]['gpo_subcat']
        return cat, subcat

    def amend(self, gpo, display=True):
        objname = gpo['gpo_name']
        cat, subcat = self.get_cats(objname)
        out = deepcopy(gpo)
        out['gpo_cat'] = cat
        out['gpo_subcat'] = subcat
        if display:
            out['gpo_cat_disp'] = self.get_display_name(cat)
            out['gpo_subcat_disp'] = self.get_display_name(subcat)
        return out

    @classmethod
    def get_display_name(cls, cat_name):
        return DISPLAY_NAMES[cat_name]

    def _get_parent_category(self, subcategory):
        outname = None
        for stname, stvals in self.obj_cats.items():
            if subcategory in stvals:
                outname = stname
                break
        if not outname:
            raise KeyError('Subcategory {} not found!'.format(subcategory))
        return outname

    def _as_table(self, sep=',\t'):
        tab = 'OBJNAME{sep}OBJCAT{sep}OBJSUBCAT'.format(sep=sep)
        for objname, vals in self.known_gpos.items():
            tab += '\n{on}{sep}{oc}{sep}{osc}'.format(
                sep=sep,
                on=objname,
                oc=vals['gpo_cat'],
                osc=vals['gpo_subcat'],
            )
        return tab

    def print(self):
        print(self._as_table())


def censor_df(df, bad_items=FORBIDDEN_ITEMS):
    out = df.copy()
    censor_idx = out[out['gpo_name'] == 'autogyro'].index
    out.drop(censor_idx, inplace=True)
    return out
