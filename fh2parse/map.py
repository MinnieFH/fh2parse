# pylint: disable=W0232,C0103,C0111,F0401
# vim:set ts=4 sts=4 sw=4 et syntax=python:
"""
Map drawing tools.

The BF2 coocrinate system has its origin 0,0 at the center of the map,
X is minus-to-plus west-east, - is west (left), + is east (right)
Y is height above ground, - is low, + is up
Z is minus-to-plus south-north, - is south (down), + is north (up)

Pillow image coordinates have the origin 0,0 at the top left of the image,
x/y are the usual (4th quadrant but with y sign flipped)
"""

from os.path import join, exists

import numpy as np
from PIL import Image, ImageDraw

from fh2parse import IMG_DIR


class MiniMap:
    """Represents a minimap, where we can draw stuff.

    Positions are 3-tuples, 2nd is ignored.

    Parameters
    ----------
    filename: str
        The base undecorated minimap from the map files.
    size: int
        Size of the ingame-/heightmap, `1 + 2^X`

    Methods
    -------
    add_marker(pos)
        Add an object at a specific location.
    draw_map(outfile_name)
        Write the map to a file, with markers applied
    transform_coord(pos)
        Go from ingame to image coordinates

    Attributes
    ----------
    img: PIL object holding the image file
    img_draw: PIL drawer of the image file
    m2i_scale: coord scale factor between image and map (`img / map`)
    img_offset: offset of the (0,0) between image and map coords

    """

    def __init__(self, filename, size):
        self.img = Image.open(filename)
        self.img_draw = ImageDraw.Draw(self.img)
        map_hw = size / 2
        # map images are quadratic
        self.img_offset = self.img.size[0] / 2
        # this needs to be added to do transform ingame->img coords
        # draw at coord+this
        self.m2i_scale = self.img_offset / map_hw
        self._icon_files = {}

    def transform_coord(self, x, y):
        """Transform game coordinates to image coordinates

        the Y coordinate here is north-south, not height above ground

        Parameters
        ----------
        pos: (float, float)
            x, y pos of an object

        Returns
        -------
        (x, y): tuple of transformed coords

        """
        # in PIL, 0/0 is top left, in BF2, 0/0 is bottom left
        # so that's why the minus on y
        x = np.atleast_1d(x)
        y = np.atleast_1d(y)
        x_new = self.img_offset + self.m2i_scale * x
        y_new = self.img_offset - self.m2i_scale * y
        return x_new, y_new

    def add_marker(self, pos, marker_size=10, marker_color=(255, 255, 0)):
        """Draw a map marker at a position, using game coords.

        Parameters
        ----------
        pos: (float, float, float)
            x, y, z pos of an object. y coord is height above ground, ignored.
        marker_size: int
            Size of marker in pixels, multiple of 2
        """
        x, z = self.transform_coord(pos[0], pos[2])
        # marker corners. draw marker centered on pos
        x_lo = x - .5 * marker_size
        x_hi = x + .5 * marker_size
        z_lo = z - .5 * marker_size
        z_hi = z + .5 * marker_size
        self.img_draw.rectangle(
            (x_lo, z_lo, x_hi, z_hi),
            fill=marker_color,
            outline=(255, 255, 255),
        )

    def add_polygon(self, points, color=(255, 255, 0)):
        points_img = []
        for pt in points:
            pos = (pt[0], 0, pt[1])
            x, z = self.transform_coord(pos[0], pos[2])
            points_img.append((x, z))
        self.img_draw.polygon(points_img, fill=color)

    def add_icon(self, pos, icon_name, icon_color=(255, 255, 0)):
        """Draw an icon at a position, using game coords.

        Parameters
        ----------
        pos: (float, float, float)
            x, y, z pos of an object. y coord is height above ground, ignored.
        icon_size: int
            Size of icon in pixels, multiple of 2
        """
        x, _, z = self.transform_coord(pos)
        icon = self._get_icon(icon_name)
        icon_size = self._get_image_size(icon)
        icon_c = recolor(icon, new_color=icon_color)
        x_lo = int(np.around(x - .5 * icon_size, decimals=0))
        # x_hi = int(np.around(x + .5 * icon_size, decimals=0))
        z_lo = int(np.around(z - .5 * icon_size, decimals=0))
        # z_hi = int(np.around(z + .5 * icon_size, decimals=0))
        upper_left = (x_lo, z_lo)
        self.img.alpha_composite(icon_c, upper_left)

    def draw_map(self, outfile_name, img_format='WEBP'):
        """Draw a map marker at a position, using game coords.

        Parameters
        ----------
        outfile_name: str
            name of file to write
        img_format: str
            Valid pillow file format
        """
        self.img.save(outfile_name, img_format)

    def crop(self, xmin, xmax, zmin, zmax):
        xmin_new = self.img_offset + self.m2i_scale * xmin
        zmin_new = self.img_offset - self.m2i_scale * zmin
        xmax_new = self.img_offset + self.m2i_scale * xmax
        zmax_new = self.img_offset - self.m2i_scale * zmax
        # (left, top, right, bottom)
        box = (xmin_new, zmax_new, xmax_new, zmin_new)
        self.img = self.img.crop(box)

    @staticmethod
    def _get_image_size(image):
        """Get maximum image dimension along either axis

        Parameters
        ----------
        image: Pillow Image

        Returns
        -------
        size: numeric
            Image size, bigger one of either width or height
        """
        size = image.size
        return max(size)

    def _get_icon(self, icon_name):
        """Load image file from disk, or, if already done so, from cache.

        Parameters
        ----------
        icon_name: str

        Returns
        -------
        ico: Pillow.Image
        """
        if icon_name in self._icon_files:
            return self._icon_files[icon_name]
        icon_fname = join(IMG_DIR, icon_name)
        if not exists(icon_fname):
            raise FileNotFoundError(
                "{} does not exist, aborting...".format(icon_fname)
            )
        ico = Image.open(icon_fname)
        self._icon_files[icon_name] = ico
        return ico


def recolor(img, old_color=(255, 255, 255), new_color=(255, 0, 0)):
    """
    Replace a color in an image with a different color.

    For example, change all white pixels into red.

    Examples
    --------
    # change all white pixels into red
    >>> recolor(img, (255, 255, 255), (255, 0, 0))

    Parameters
    ----------
    img: PIL.Image
        The image to recolor
    old_color: tuple(int)
        rgb tuple (white is `(255, 255, 255)`)
    new_color: tuple(int)
        rgb tuple for new color (white is `(255, 255, 255)`)
    """
    np_img = img.convert('RGBA')
    data = np.array(np_img)
    # pylint doesn't understand that .T *can* be unpacked
    r, g, b, _ = data.T  # pylint: disable=W0633

    oc_areas = (r == old_color[0]) & (b == old_color[1]) & (g == old_color[2])
    data[..., :-1][oc_areas.T] = new_color
    return Image.fromarray(data)


def bounding_rectangle(points, square=False):
    """Compute the hull rectangle that encloses all the points.


    Parameters
    ----------
    points: list(2-tuple)
    square: bool
        crop it as a square

    Returns
    -------
    square: 4-tuple
        left, upper, right, lower. 0/0 is upper left
    """
    xmin, ymin = np.min(points, axis=0)
    xmax, ymax = np.max(points, axis=0)
    # left, upper, right, lower
    if square:
        _min = min(ymin, xmin)
        _max = max(ymin, xmin)
        xmax = _max
        ymax = _max
        xmin = _min
        ymin = _min
    return (xmin, ymin, xmax, ymax)
