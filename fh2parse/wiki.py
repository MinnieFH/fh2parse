# pylint: disable=W0232,C0103,C0111
# vim:set ts=4 sts=4 sw=4 et syntax=python:
"""
Generate Wiki pages
"""

from textwrap import dedent

from fh2parse.parse import read_description


def html_gallery(image_fnames):
    html_out = ""
    for ifn, disp in image_fnames.items():
        html_out += """
<div class="gallery">
<a target="_blank" href="{img}">
<img src="{img}" alt="{desc}" width="900" height="600">
</a>
<div class="desc">{desc}</div>
</div>
""".format(img=ifn, desc=disp)
    return html_out
