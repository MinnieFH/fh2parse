from unittest import TestCase

from fh2parse.gpo import GPOTable
from fh2parse import GPO_CSV


class TestGPOTable(TestCase):

    def setUp(self):
        self.gpo_table = GPOTable(GPO_CSV)

    def test_good_cats(self):
        sample_gpo = {
            'instance': 'cp_64_pegasus_pegasus_bridge_ammo',
            'gpo_name': 'bw_pickupammokit',
            'is_locked': False,
            'team': 0,
            'pos_x': 27.575,
            'pos_y': 24.917,
            'pos_z': 163.985,
            'flag': 106
        }
        cat_, subcat_ = self.gpo_table.get_cats(sample_gpo['gpo_name'])
        sample_gpo['category'] = cat_
        sample_gpo['subcategory'] = subcat_
        assert cat_ == 'kit'
        assert subcat_ == 'ammo'

    def test_amend(self):
        sample_gpo = {
            'instance': 'cp_64_pegasus_pegasus_bridge_ammo',
            'gpo_name': 'bw_pickupammokit',
            'is_locked': False,
            'team': 0,
            'pos_x': 27.575,
            'pos_y': 24.917,
            'pos_z': 163.985,
            'flag': 106
        }
        new_gpo = self.gpo_table.amend(sample_gpo)
        assert 'gpo_cat' in new_gpo
        assert 'gpo_cat' not in sample_gpo
        assert 'gpo_subcat' in new_gpo
        assert 'gpo_subcat' not in sample_gpo

    def test_bad_item(self):
        bad_item = 'some_nonexisting_object'
        cat, subcat = self.gpo_table.get_cats(bad_item)
        assert cat == 'noidea'
        assert subcat == 'noidea'

    def test_raise_on_bad_category(self):
        bad_subcat = 'bullshit'
        with self.assertRaises(KeyError):
            self.gpo_table._get_parent_category(bad_subcat)

    def test_ammo(self):
        ammo = {
            'instance': 'ammo_crate_5',
            'gpo_name': 'ammo_crate',
            'is_locked': False,
            'team': 0,
            'pos_x': -10.0,
            'pos_y': 20.0,
            'pos_z': 30.0,
            'flag': 0,
        }
        cat, subcat = self.gpo_table.get_cats(ammo['gpo_name'])
        assert cat == 'ammo_crate'
        assert subcat == 'ammo_crate'
        ammo_new = self.gpo_table.amend(ammo)
        assert ammo_new['gpo_cat'] == 'ammo_crate'
        assert ammo_new['gpo_subcat'] == 'ammo_crate'

    def test_print(self):
        self.gpo_table.print()
