# pylint: disable=W0232,C0103,C0111
# vim:set ts=4 sts=4 sw=4 et syntax=python:

from os.path import join, dirname
from pathlib import Path

DATA_DIR = join(Path(dirname(__file__)).parent, 'data')
DESCRIPTION_LIST_FILE = join(DATA_DIR, 'maps_fh.trim.txt')
MAP_LIST_FILE = join(DATA_DIR, 'mapnames.txt')
PAGE_DIR = join(Path(dirname(__file__)).parent, 'pages')
LEVEL_DIR = join(Path(dirname(__file__)).parent, 'data', 'levels')
GPO_CSV = join(Path(dirname(__file__)).parent, 'data', 'gpo_lookup.csv')
IMG_DIR = join(Path(dirname(__file__)).parent, 'data', 'img')

MAP_LAYERS = {
    'cmp_raid_on_cabantuan': {
        '16',
        '32',
        '64',
    },
    'el_alamein': {
        '16',
        '32',
        '64',
    },
    'tunis_1943': {
        '16',
        '64',
    },
    'omaha_beach': {
        '16',
        '64',
    },
    'sidi_rezegh': {
        '16',
        '32',
        '64',
    },
    'lebisey': {
        '16',
        '64',
    },
    'operation_cobra': {
        '16',
        '32',
        '64',
    },
    'mareth_line': {
        '16',
        '32',
        '64',
    },
    'ihantala': {
        '16',
        '32',
        '64',
        'airdomination',
    },
    'crete_1941': {
        '16',
        '32',
        '64',
    },
    'motovskiy_bay': {
        '16',
        '32',
        '64',
    },
    'lenino': {
        '16',
        '32',
        '64',
    },
    'villers_bocage': {
        '32',
        '64',
    },
    'meuse_river': {'64', },
    'vossenack': {
        '16',
        '64',
    },
    'gazala': {
        '16',
        '32',
        '64',
    },
    'bastogne': {
        '16',
        '32',
        '64',
    },
    'mersa_matruh': {
        '16',
        '32',
        '64',
    },
    'cmp_midway_1942': {'64', },
    'hurtgen_forest': {
        '16',
        '32',
        '64',
    },
    'fall_of_tobruk': {
        '16',
        '64',
    },
    'giarabub': {
        '16',
        '32',
        '64',
    },
    'eppeldorf': {
        '16',
        '32',
        '64',
    },
    'anctoville_1944': {
        '16',
        '32',
        '64',
    },
    'studienka': {
        '16',
        '32',
        '64',
    },
    'cmp_irrawaddy_river': {
        '16',
        '32',
        '64',
    },
    'st_lo_breakthrough': {
        '32',
        '64',
    },
    'st_vith': {
        '16',
        '32',
        '64',
    },
    'cmp_tarawa': {
        '16',
        '32',
        '64',
    },
    'operation_luttich': {
        '16',
        '64',
    },
    'bardia': {
        '16',
        '64',
    },
    'cmp_firing_range': {'64', },
    'cmp_wake_island': {
        '16',
        '32',
        '64',
    },
    'falaise_pocket': {
        '16',
        '64',
    },
    'operation_totalize': {
        '16',
        '32',
        '64',
    },
    'ramelle': {'64', },
    'siege_of_tobruk': {
        '16',
        '32',
        '64',
    },
    'purple_heart_lane': {
        '16',
        '64',
    },
    'pointe_du_hoc': {
        '32',
        '64',
    },
    'alam_halfa': {
        '16',
        '32',
        '64',
    },
    'battle_of_keren': {
        '16',
        '32',
        '64',
    },
    'tali': {
        '16',
        '32',
        '64',
    },
    'operation_hyacinth': {
        '16',
        '64',
    },
    'battle_of_brest': {
        '16',
        '64',
    },
    'gold_beach': {'64', },
    'ogledow': {
        '16',
        '32',
        '64',
    },
    'supercharge': {
        '16',
        '32',
        '64',
    },
    'the_battle_for_sfakia': {
        '16',
        '64',
    },
    'mount_olympus': {
        '16',
        '32',
        '64',
    },
    'cmp_tulagi': {
        '16',
        '32',
        '64',
    },
    'operation_goodwood': {
        '16',
        '64',
    },
    'sammatus': {
        '16',
        '64',
    },
    'firing_range': {
        '16',
        '32',
        '64',
    },
    'arad': {
        '16',
        '32',
        '64',
    },
    'seelow': {
        '16',
        '32',
        '64',
    },
    'sidi_bou_zid': {
        '16',
        '32',
        '64',
    },
    'cmp_berlin': {
        '16',
        '32',
        '64',
    },
    'port_en_bessin': {
        '16',
        '64',
    },
    'dukla_pass': {
        '16',
        '32',
        '64',
    },
    'pegasus': {
        '16',
        '32',
        '64',
    },
}
